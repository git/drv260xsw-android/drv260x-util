/* ************************************************************************
 *       Filename:  drv2603.c
 *    Description:  
 *        Version:  1.0
 *        Created:  05/08/2020 03:53:39 PM
 *       Revision:  none
 *       Compiler:  gcc
 *         Author:  YOUR NAME (), 
 *        Company:  
 * ************************************************************************/

#define DEBUG
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/semaphore.h>
#include <linux/device.h>
#include <linux/syscalls.h>
#include <asm/uaccess.h>
#include <linux/gpio.h>
#include <linux/sched.h>
#include <linux/spinlock_types.h>
#include <linux/spinlock.h>
#include <linux/delay.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/jiffies.h>
#include <linux/err.h>
#include <linux/clk.h>
#include <linux/miscdevice.h>
#include <linux/interrupt.h>
#include <linux/cdev.h>
#include "timed_output.h"
struct drv2603_data {
	struct mutex lock;
	struct mutex dev_lock;
	struct cdev cdev;
	struct hrtimer haptics_timer;
	struct timed_output_dev to_dev;
};
static int vibrator_get_time(struct timed_output_dev *dev)
{
	struct drv2603_data *pDRV2603 =
	    container_of(dev, struct drv2603_data, to_dev);
	if (hrtimer_active(&pDRV2603->haptics_timer)) {
		ktime_t r = hrtimer_get_remaining(&pDRV2603->haptics_timer);
		return ktime_to_ms(r);
	}
	return 0;
}

static void vibrator_enable(struct timed_output_dev *dev, int value)
{
	struct drv2603_data *pDRV2603 = container_of(dev, struct drv2603_data, to_dev);
	dev_err(pDRV2603->to_dev.dev, "%s, value=%d\n", __func__, value);
}

static enum hrtimer_restart vibrator_timer_func(struct hrtimer *timer)
{
	struct drv2603_data *pDRV2603 =
	    container_of(timer, struct drv2603_data, haptics_timer);
	dev_err(pDRV2603->to_dev.dev, "%s\n", __func__);
	return HRTIMER_NORESTART;
}

static ssize_t drv2603_activate_store(struct device *dev, struct device_attribute *attr,
			     const char *buf, size_t size)
{
	struct timed_output_dev *timed_dev = dev_get_drvdata(dev);
	struct drv2603_data *pDRV2603 = container_of(timed_dev, struct drv2603_data, to_dev);
	dev_err(pDRV2603->to_dev.dev,"%s:enter!\n", __func__);
	return size;
}
static ssize_t drv2603_activate_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct timed_output_dev *timed_dev = dev_get_drvdata(dev);
	struct drv2603_data *pDRV2603 = container_of(timed_dev, struct drv2603_data, to_dev);
	dev_err(pDRV2603->to_dev.dev,"%s:enter!\n", __func__);
	return 0;

}
static DEVICE_ATTR(activate, 0664, drv2603_activate_show, drv2603_activate_store);
static struct attribute *drv2603_vibrator_attributes[] = {
	&dev_attr_activate.attr,
	NULL
};

static struct attribute_group drv2603_vibrator_attribute_group = {
	.attrs = drv2603_vibrator_attributes
};

static int Haptics_init(struct drv2603_data *pDRV2603)
{
	int nResult = 0;
	printk("%s,%d\n",__func__,__LINE__);
	pDRV2603->to_dev.name = "vibrator";
	pDRV2603->to_dev.get_time = vibrator_get_time;
	pDRV2603->to_dev.enable = vibrator_enable;
	nResult = timed_output_dev_register(&(pDRV2603->to_dev));
	if (nResult < 0) {
		dev_err(pDRV2603->to_dev.dev,
			"drv2603: fail to create timed output dev\n");
		return nResult;
	}
	hrtimer_init(&pDRV2603->haptics_timer, CLOCK_MONOTONIC,
		     HRTIMER_MODE_REL);
	pDRV2603->haptics_timer.function = vibrator_timer_func;

	mutex_init(&pDRV2603->lock);
	return 0;
}
static int drv2603_parse_dt(struct drv2603_data *pDRV2603)
{
	struct device_node *cgnp;
	int nResult = 0;
	printk("%s:enter!\n", __func__);
	cgnp = of_find_compatible_node(NULL, NULL, "ti_tas2603");
	nResult = of_get_named_gpio(cgnp, "ti,reset-gpio", 0);
	dev_err(pDRV2603->to_dev.dev, "%s:ti,reset-gpio=%d\n", __func__, nResult);
	return nResult;
}
int drv2603_init(void)
{
	struct drv2603_data *pDRV2603;
	pDRV2603 = kzalloc(sizeof(struct drv2603_data), GFP_KERNEL);
	if (pDRV2603 == NULL) {
		kfree(pDRV2603);
		printk("%s,%d kzalloc fail .\n",__func__,__LINE__);
	}
	Haptics_init(pDRV2603);
	dev_err(pDRV2603->to_dev.dev,"%s:enter!\n", __func__);
	sysfs_create_group(&pDRV2603->to_dev.dev->kobj,
			   &drv2603_vibrator_attribute_group);
	drv2603_parse_dt(pDRV2603);
	return 0;
}

void drv2603_exit(void)
{
	struct drv2603_data *pDRV2603;
	pDRV2603 = kzalloc(sizeof(struct drv2603_data), GFP_KERNEL);
	dev_err(pDRV2603->to_dev.dev,"%s:enter!", __func__);
	timed_output_dev_unregister(&(pDRV2603->to_dev));
	kfree(pDRV2603);
}
MODULE_LICENSE("Dual BSD/GPL");
module_init(drv2603_init);
module_exit(drv2603_exit);



